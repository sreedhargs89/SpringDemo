package com.sree.spring.point;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PointDrawingApp {
	
	private static ApplicationContext context;

	public static void main(String[] args) {

		context = new ClassPathXmlApplicationContext("DependencySpring.xml");
		/**
		 * Creating the PointTriangle object via, Spring description xml provided ie DependencySpring.xml
		 */
		PointTriangle pointtriangle = (PointTriangle)context.getBean("pointtriangle");
		pointtriangle.draw();
		
		/*
		This snippet helps to test the creation of the Point Objects
		Point point = (Point) context.getBean("point3");
		System.out.println(point);
		*/
	}

}
