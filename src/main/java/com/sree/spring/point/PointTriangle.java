package com.sree.spring.point;

public class PointTriangle {

	private Point pointA;
	private Point pointB;
	private Point pointC;
	
	
	public PointTriangle(Point pointA, Point pointB, Point pointC) {
		super();
		this.pointA = pointA;
		this.pointB = pointB;
		this.pointC = pointC;
	}
	
	public PointTriangle() {}
	
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}
	
	
	public void draw() {
		System.out.println("Drawing Triangle:\n PointA "+getPointA() +"\n PointB "+getPointB() +
				"\n PonitC "+getPointC());
	}

}
