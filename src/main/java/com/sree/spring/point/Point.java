package com.sree.spring.point;

public class Point {
	
	private String x;
	private String y;
	
	public Point(String x, String y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Point() {
		super();
	}


	public String getX() {
		return x;
	}
	public void setX(String x) {
		this.x = x;
	}
	public String getY() {
		return y;
	}
	public void setY(String y) {
		this.y = y;
	}
	
	
	@Override
	public String toString() {
			return getX() +" , "+getY();
	}

}
